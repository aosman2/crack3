#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be


// Stucture to hold both a plaintext password and a hash.
struct entry {
    char hash[100];
    char password[100];
};

// TODO
// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size) {
    struct entry *entries;
    FILE *dfile;
    int i;
    char line[101];
    dfile = fopen(filename, "r"); 
    *size = 0;
    
    while (fgets(line, 100, dfile)) {
       *size = *size + 1;
    }

    // allocate memory using malloc()
    entries = (struct entry *)malloc(*size * sizeof (struct entry))
    
    // read the file line by line
    i = 0;
    while (fgets(line, 100, dfile)) {
        strcpy(entries[i].password, line)
        strcpy(entries[i].hash, md5(line, strlen(line));
    }

    fclose(dfile);


    return entries; 
}

int main(int argc, char *argv[]) {
    char line[101];
    
    if (argc < 3) {
        printf("Usage: %s hash_file dict_file", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    struct entry *dict = read_dictionary(argv[2], &size);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function that
    // sorts the array by hash value.

    int (*compareEntries)(const void * pv1, const void*pv2) {
    struct entry *entry1 = (struct entry *) pv1;
    struct entry *entry2 = (struct entry *) pv2;
    return strcmp(entry1->hash, entry2->hash);

    qsort(dict, size, sizeof(struct entry), compareEntries);

    // TODO
    // Open the hash file for reading.
    FILE *hfile;
    hfile = fopen(argv[1], "r");

    struct entry e;
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    while (fgets(line, 100, hfile)) {
        strcpy(e.hash, line);
        struct entry *found = bsearch(&e, dict, size, sizeof(struct entry), compareEntries);
       
        // If you find it, get the corresponding plaintext dictionary word.
        // Print out both the hash and word.
        // Need only one loop. (Yay!)
        if (found != NULL) {
            printf("%s\n", found->password);
            printf("%s\n", found->hash)
        }
        else {
            print("Not found\n")
        }
    }
    fclose(hfile;)
    return 0; 
}


